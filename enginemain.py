from typing import Union
from fastapi import FastAPI
from datamike import data

app = FastAPI()


@app.get("/")
def index():
    return data


@app.get("/items/{item_id}")
def read_item(item_id: str):
    try:
        for key, value in data.items():
            if item_id == key:
                return value

            else:
                return {'Mensaje' : "NO se encontró registro"}

    except:
        return {'Mensaje' : "NO se encontró registro"}





